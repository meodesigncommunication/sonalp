$(function(){

    $(window).scrollTop(0);

    $(window).scroll(function(){
        if($(this).scrollTop() == 0) {
            $('#header').removeClass('scrolled-header');
        }else{
            $('#header').addClass('scrolled-header');
        }
    });

    $('.scrollTo').on('click', function() { // Au clic sur un élément
        var page = $(this).attr('href'); // Page cible
        var speed = 800; // Durée de l'animation (en ms)
        $('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
        if($('#navbar').css('display') == 'block' || $('#navbar').css('display') == 'flex'){
            if($('#navbar i.fa-close').css('display') == 'block'){
                closeNavigation();
            }
        }
        return false;
    });

});