$(function(){

    resizeModularImgText();
    resizeHeightNav();

    $(window).resize(function(){
        resizeModularImgText();
        resizeHeightNav();
    });


   function resizeModularImgText(){
       if($(window).width() <= '736'){
           $('section.imgtext').each(function(){
               var height = $(this).find('div:last-child').height();
               $(this).find('div:first-child').height(height);
           });
           $('section.icon').each(function(){
               var height = $(this).find('div:last-child').height();
               $(this).find('div:first-child').height(height);
           });
       }
   }

    function resizeHeightNav(){
        if($('#navbar').css('display') != 'none'){
            var height = $(window).height();
            $('header ul.navigation').height(height);
            if($('ul.navigation').css('display') == 'flex'){
                $('ul.navigation').css('display','none');
                $('#navbar i.fa-bars').css('display', 'block');
                $('#navbar i.fa-close').css('display', 'none');
            }
        }else{
            $('header ul.navigation').height('auto');
            if($('ul.navigation').css('display') == 'none' || $('ul.navigation').css('display') == 'block'){
                $('ul.navigation').css('display','flex');
            }
        }
    }
});