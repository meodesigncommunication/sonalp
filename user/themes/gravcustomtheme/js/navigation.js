function openNavigation() {
    $('#navbar i.fa-bars').css('display', 'none');
    $('#navbar i.fa-close').css('display', 'block');
    $('ul.navigation').css('display', 'block');
}

function closeNavigation() {
    $('#navbar i.fa-bars').css('display', 'block');
    $('#navbar i.fa-close').css('display', 'none');
    $('ul.navigation').css('display', 'none');
}