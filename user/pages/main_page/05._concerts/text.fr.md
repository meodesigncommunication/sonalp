---
title: Concerts
color: '#5f4394'
---

## Concerts

Dimanche 17 juin 2018<br/>
Fête de la musique<br/>
Bulle – 19h00<br/>
[www.fmbulle.ch](http://fmbulle.ch/?target=_blank)

-

Dimanche 24 juin 2018<br/>
Lenzburg (Argovie), Metzgplatz – 19h30<br/>
[www.lenzburgiade.ch](https://www.lenzburgiade.ch/?target=_blank)

-

Samedi 28 juillet 2018<br/>
Festival Musique Montagne<br/>
Les Diablerets – 20h00<br/>
[www.musique-montagne.com](http://www.musique-montagne.com/?target=_blank)

-

Mardi 31 juillet 2018<br/>
Morlon Beach - Au bord du lac de Gruyère<br/>
Morlon – 20h00<br/>
[www.morlonbeach.ch](http://www.morlonbeach.ch/?target=_blank)

-

Dimanche 16 septembre 2018<br/>
Paillote Festival<br/>
Parc de l'Indépendance - Morges – 20h30<br/>
[www.paillote-festival.ch](http://www.paillote-festival.ch/?target=_blank)

D'autres dates à venir