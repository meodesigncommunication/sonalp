---
title: Concerts
color: '#5f4394'
---

## Concerts
Sunday 17 june 2018<br/>
Fête de la musique<br/>
Bulle – 19h00<br/>
[www.fmbulle.ch](http://https://fmbulle.ch/?target=_blank)

-

Sunday 24 june 2018<br/>
Lenzburg (Argovie), Metzgplatz – 19h30<br/>
[www.lenzburgiade.ch](https://www.lenzburgiade.ch/?target=_blank)

-

Saturday 28 july 2018<br/>
Festival Musique Montagne<br/>
Les Diablerets – 20h00<br/>
[www.musique-montagne.com](http://www.musique-montagne.com/?target=_blank)

-

Tuesday 31 july 2018<br/>
Morlon Beach<br/>
Morlon – 20h00<br/>
[www.morlonbeach.ch](http://www.morlonbeach.ch/?target=_blank)

-

Sunday 16 septembre 2018<br/>
Paillote Festival<br/>
Parc de l'Indépendance - Morges – 20h30<br/>
[www.paillote-festival.ch](http://www.paillote-festival.ch/?target=_blank)

Other dates to follow.