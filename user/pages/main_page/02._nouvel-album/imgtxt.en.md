---
title: 'New album'
image_align: right
image:
    user/medias/Sonalp_nouvel_album.jpg:
        name: Sonalp_nouvel_album.jpg
        type: image/jpeg
        size: 935672
        path: user/medias/Sonalp_nouvel_album.jpg
---

## terre à terres
We have always proudly represented a modern, multicultural Switzerland, open to the world while attached to its roots. The album Terre à terre embodies these values, in its name as well as its music (in French this translates as “from the earth to the land”, but it’s a phrase that also means “down to earth”). From one earth to many different lands … it’s an invitation to respect our shared planet and everyone who lives on it.

The new album is available from 5 January 2018 at Fnac, Manor, good record shops and online outlets [cede.ch](https://www.cede.ch/en/music/?view=detail&branch_sub=0&branch=1&aid=16149014&target=_blank) and [exlibris.ch](https://www.exlibris.ch/fr/musique/musique-cd/sonalp/terre-a-terres/id/7611745659663?target=_blank) 

You can find it on  [iTunes](https://itunes.apple.com/ch/album/terre-%C3%A0-terres/1327234139?l=fr&target=_blank) !