---
title: 'Nouvel Album'
image_align: right
image:
    user/medias/Sonalp_nouvel_album.jpg:
        name: Sonalp_nouvel_album.jpg
        type: image/jpeg
        size: 935672
        path: user/medias/Sonalp_nouvel_album.jpg
---

## terre à terres
Nous avons toujours été fier de représenter une Suisse moderne, multiculturelle, ouverte sur le monde et attachée à ses racines.<br/>
L’album « Terre à terres » réunit dans son intitulé déjà les fondamentaux de ces valeurs. Une seule Terre à plusieurs terres… invitant au partage et au respect de la planète et de tous ceux qui la peuplent.

Le nouvel album est disponible dès le 5 janvier 2018 en magasin, à la Fnac, Manor, les bons disquaires ainsi que sur les shops en ligne [cede.ch](https://www.cede.ch/fr/music/?view=detail&branch_sub=0&branch=1&aid=16149014&target=_blank) et [exlibris.ch](https://www.exlibris.ch/fr/musique/musique-cd/sonalp/terre-a-terres/id/7611745659663?target=_blank) 

Egalement en ligne sur [iTunes](https://itunes.apple.com/ch/album/terre-%C3%A0-terres/1327234139?l=fr&target=_blank) !