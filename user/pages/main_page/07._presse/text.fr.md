---
title: Presse
media_order: SONALP_communique_presse_sonalp.pdf
color: '#c2734b'
---

## Presse
Vous avez envie de partager avec vos lecteurs et auditeurs la nouvelle aventure de Sonalp - terre à terres ?<br/> 
Téléchargez le communiqué de presse et nous nous ferons un plaisir de répondre à toutes vos questions ! 

[Communiqué de presse](SONALP_communique_presse_sonalp.pdf?target=_blank?target=_blank)