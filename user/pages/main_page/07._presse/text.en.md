---
title: Press
media_order: SONALP_communique_presse_sonalp_EN.pdf
color: '#c2734b'
---

## Press
Do you want to share Sonalp’s latest adventure with your readers and listeners – straight from the horse’s mouth ?<br/> 
Download the press release and we’ll try to answer all your questions ! 

[Press release](SONALP_communique_presse_sonalp_EN.pdf?target=_blank)