---
title: Accueil
media_order: Sonalp_pics_accueil.jpg
slider:
    -
        slide_name: 'slider 1'
        slide_pic: Sonalp_pics_accueil.jpg
        slide_vertical_pos: center
        slide_horiz_pos: center
buttons:
    -
        text: 'contacter nous'
        url: contact
        primary: true
---

<span class="new-album">NOUVEL ALBUM</span>
<span class="title-album">terre à terres</span>
<span class="description-album">En magasin, sur les plateformes<br/>numériques et shops en ligne dès le<br/>**5 janvier 2018**</span>