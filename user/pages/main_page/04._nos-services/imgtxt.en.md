---
title: Musicians
image_align: left
image:
    user/medias/Sonalp_Musicien.jpg:
        name: Sonalp_Musicien.jpg
        type: image/jpeg
        size: 685938
        path: user/medias/Sonalp_Musicien.jpg
---

## Musiciens
### Chris
didgeridoo, alphorn, cowbells, bowl, harmony vocals
### Guillaume
violin, singing saw, bass guitar
### John
hang, keyboards, electronica, djembé
### Laurent
bass guitar, guitar, ukelele, double bass, voice, yodel and lead vocal
### Marc
Voice, yodel, vocals, accordion and cowbells
### Rémy
Drums, djembé 