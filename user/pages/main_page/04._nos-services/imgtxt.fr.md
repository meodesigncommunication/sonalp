---
title: Musiciens
image_align: left
image:
    user/medias/Sonalp_Musicien.jpg:
        name: Sonalp_Musicien.jpg
        type: image/jpeg
        size: 685938
        path: user/medias/Sonalp_Musicien.jpg
---

## Musiciens
### Chris
didgeridoo, cor des Alpes, cloches, bol, voix harmoniques
### Guillaume
violon, scie musicale, guitare basse
### John
hang, claviers, électro, djembé
### Laurent
guitare basse, guitare, ukulélé, contrebasse, voix, jodle et chant
### Marc
voix, jodle, chant, accordéon et cloche
### Rémy
batterie, djembé