---
title: Contact
form:
    name: contact-form
    action: /
    fields:
        -
            name: first_name
            label: First name
            placeholder: 'First name*'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            outerclasses: field_name
            validate:
                required: true
        -
            name: last_name
            label: Surname
            placeholder: 'Surname*'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            outerclasses: field_name
            validate:
                required: true
        -
            name: email
            label: Email
            placeholder: 'Email*'
            type: email
            validate:
                required: true
        -
            name: phone
            label: Phone
            placeholder: 'Phone*'
            type: text
            validate:
                required: true
        -
            name: country
            label: Pays
            type: select
            data-options@: '\Grav\Theme\Gravcustomtheme::countryCodes'
            validate:
                required: true
        -
            name: message
            label: Message
            autofocus: 'on'
            type: textarea
            validate:
                required: true
        -
            name: g-recaptcha-response
            label: Captcha
            type: captcha
            recaptcha_site_key: 6LcfKz8UAAAAALw8gCYBmpNzqG2AX6za3HKJesQu
            recaptcha_not_validated: 'Captcha not valid!'
            validate:
                required: true
    buttons:
        -
            type: submit
            value: Send
    process:
        -
            captcha:
                recatpcha_secret: 6LcfKz8UAAAAAGrMNsVRdBoXSYL7zpSWSXhTrzeG
        -
            email:
                from_name: '{{ form.value.first_name|e }} {{ form.value.last_name|e }}'
                from: '{{ form.value.email }}'
                to: [info@sonalp.ch, john@sonalp.ch]
                subject: 'Sonalp.com | Demande de contact | {{ form.value.first_name|e }} {{ form.value.last_name|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Thank you, we will contact you soon as possible !'
---

**Lorem ipsum dolor sit**
<br/>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.