first_name: Marianne
last_name: Lathion
email: lathion.marianne@outlook.fr
phone: '079 581 94 42'
country: CH
message: "Bonjour \"Sonalp\". J'ai découvert votre groupe et trouve génial ce que vous faites. Bravo!!! \r\nJe chante dans une chorale dans le Valais central, a Veysonnaz. Nous organisons le 1er week-end de mai 2019 un festival de chant de l'union chorale du centre. A cette occasion nous avons coutume d'organiser une soirée avec spectacle . Nous sommes très intéressés par votre groupe. \r\nCette soirée aura lieu au milieu de la station, sous cantine. \r\nVotre participation nous enchanterait. Merci de me répondre rapidement si ce projet vous intéresse, et de m'indiquer qu'elles seraient vos conditions.\r\nDans l'attente de vos nouvelles, je vous transmets au nom du comité nos meilleures salutations\r\nMarianne Lathion"

